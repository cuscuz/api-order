FROM maven:3.8-openjdk-11
COPY . .
COPY ./newrelic/newrelic.jar ./newrelic/newrelic.jar
RUN mvn package -Dmaven.test.skip -DskipTests -Dmaven.javadoc.skip=true
EXPOSE 8087
CMD [ "java","-javaagent:/newrelic/newrelic.jar","-jar","./target/group5-0.0.1-SNAPSHOT.jar" ]
