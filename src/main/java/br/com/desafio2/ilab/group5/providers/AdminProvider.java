package br.com.desafio2.ilab.group5.providers;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;


@Service
public class AdminProvider {
    private final static String path = System.getenv("URL_API_ADMIN");
    private static WebClient webClient = WebClient.create(path);

	public static HttpStatus validateToken(String token) { 
		try { 	
			Mono<ResponseEntity<String>> response = webClient
				.method(HttpMethod.GET)
				.uri("/validateToken")
				.accept(MediaType.APPLICATION_JSON)
				.header("Authorization", token)
			    .retrieve()
			    .toEntity(String.class); 
			return response.block().getStatusCode();
			
		} catch (WebClientResponseException e) { 
			return e.getStatusCode();
		}
	}
}
