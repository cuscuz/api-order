package br.com.desafio2.ilab.group5.providers;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;

import br.com.desafio2.ilab.group5.model.User;
import reactor.core.publisher.Mono;

public class UserProvider {

	private final static String baseUrl = System.getenv("URL_API_USER");
    private static WebClient webClient = WebClient.create(baseUrl);
	
	public static User getUser(int id, String token) {
		try { 	
			Mono<User> monoUser = webClient
				.method(HttpMethod.GET)
				.uri("users/{id}", id)
				.header("Authorization", token)
			    .retrieve()
			    .bodyToMono(User.class); 
			
			return monoUser.block(); 
		} catch (Exception e) { 
			if (e.getMessage().contains("404")) { 
				return null; 
			} else { 
				e.printStackTrace();
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Não foi possível conectar ao serviço de usuários.");
			}
		}
	}
	
}
